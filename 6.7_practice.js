/*console.log("Задача 1");
function getOlderUser(user1, user2) {
    if (user1.age > user2.age) {
        return user1.name;
    }
    return user2.name;
}
const user1 = {
    name: 'Игорь',
    age: 17
}
const user2 = {
    name: 'Оля',
    age: 21
}
// Вызов созданной функции
const result = getOlderUser(user1, user2)
console.log(result);
*/

console.log("Задача 2");
function getOlderUser(mas) {
    let array = [];
    for (let user of mas) {
        array.push(user.age);
    }
    return mas.filter(e => e.age >= Math.max(...array))[0].name
}
const allUsers = [
    { name: 'Валя', age: 11 },
    { name: 'Таня', age: 24 },
    { name: 'Рома', age: 21 },
    { name: 'Надя', age: 34 },
    { name: 'Антон', age: 7 }
]
console.log(getOlderUser(allUsers));

console.log("Задача 3")
function filter(mas, key, value) {
    return mas.filter(e => e[key] == value);
}
const objects = [
    { name: 'Василий', surname: 'Васильев' },
    { name: 'Иван', surname: 'Иванов' },
    { name: 'Пётр', surname: 'Петров' }
]
const result = filter(objects, 'name', 'Иван');
console.log(result);