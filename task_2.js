const studentObj = {
    name: 'Игорь',
    age: 17
}
function createStudentCard(student) {
    const div = document.createElement('div');
    const h2 = document.createElement('h2');
    const span = document.createElement('span');

    document.body.append(div);
    div.append(h2);
    div.append(span);

    h2.textContent = student.name;
    span.textContent = `Возраст: ${student.age} лет`;
}

createStudentCard(studentObj);